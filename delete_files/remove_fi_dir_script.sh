#!/bin/sh

# Abdelrahman Rezk 12 شعبان 1440

# you can use this script if you have any number of files but you need to delete 
# only files with specific name but confusing to scroll over all of them and see where files has the name you need to delete

# script to remove files - directories depends on second parameter after name of script
# also you can leave second parameter if you need to remove only files 
# third parameter is required to delete files  depends on their names
	# ./name_of_file -r file
	# above example explantion 
	# 1- ./executed what next using terminal you need to clone file in directory
	#  you need to remove files from or of you familiar with terminal you can use from any where
	# 2- name_of_file you have cloned
	# 3- optional parameter after space provide 
		# r for remove only files rf remove all files and directories

 # script structure
	# --- first to execute
		#--  chmod of file if you face a permission denied chmod +x name_of_file
		# ./name_of_file first param is optional second param is required
		#e.x ./file_name r file

	

# script to delete files by specific text in their name 
#depends on what you provide r is delete files only -rd 
# delete files and directory that has this text in thier name



# /*
# Delete file that contain a specific text in their names 
# also we can do it using two commands below but one of them is to delete all of files under this directory and others sub directory just files but second command is also remove the files and directory
# helpful link
# https://www.ostechnix.com/find-delete-files-contains-specific-text-names-linux*/

# --------- find -type f -name '*text that commaon of files*' -delete

# ----------rm *common text in file* for files rm -rf *common text in file*


if [ $# -eq 1 ]; then
	common_text_name_to_delete_related_files=$1  # text in file name like land-en 
	rm  *$common_text_name_to_delete_related_files*
	echo "files contain this name '$common_text_name_to_delete_related_files' are deleted succfuly"
elif [ $# -eq 2 ]; then
	optional_removed=$1 # provide r or rf for files and (files or directories)
	common_text_name_to_delete_related_files=$2
	rm  -$optional_removed *$common_text_name_to_delete_related_files*  # text in file name like land-en
	echo "files and directories contain this name '$common_text_name_to_delete_related_files' are deleted succfuly"
fi